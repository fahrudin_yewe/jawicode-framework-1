/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : jawicode1

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 03/05/2021 10:59:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori`  (
  `id_kat` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `for_modul` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_kat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES (1, 'Kabupaten Temanggung', '-', 'daerah_pemasaran');
INSERT INTO `kategori` VALUES (2, 'Ingin hamil/anak', '-', 'ALASAN_PUTUS_KB');
INSERT INTO `kategori` VALUES (3, 'Tidak tahu tentang KB', '-', 'ALASAN_PUTUS_KB');
INSERT INTO `kategori` VALUES (4, 'MOW/Steril Wanita', '-', 'KB_YANG_DIPAKAI');
INSERT INTO `kategori` VALUES (5, 'MOP/Steril Pria', '-', 'KB_YANG_DIPAKAI');

-- ----------------------------
-- Table structure for master_access
-- ----------------------------
DROP TABLE IF EXISTS `master_access`;
CREATE TABLE `master_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_access` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_access
-- ----------------------------
INSERT INTO `master_access` VALUES (1, 'M_USERS', 'MENU USER', '0000-00-00 00:00:00', 0);
INSERT INTO `master_access` VALUES (4, 'M_LAPORAN', 'MENU LAPORAN', NULL, NULL);
INSERT INTO `master_access` VALUES (5, 'M_SY_CONFIG', 'MENU SISTEM', '0000-00-00 00:00:00', 0);
INSERT INTO `master_access` VALUES (16, 'M_SY_CONFIG', '', '2019-06-12 07:48:03', 1);
INSERT INTO `master_access` VALUES (51, 'M_SY_CONFIG', '', '2020-01-06 12:30:09', 1);
INSERT INTO `master_access` VALUES (52, 'C_SY_CONFIG', '', '2020-01-06 12:30:20', 1);
INSERT INTO `master_access` VALUES (53, 'U_SY_CONFIG', '', '2020-01-06 12:30:29', 1);
INSERT INTO `master_access` VALUES (54, 'D_SY_CONFIG', '', '2020-01-06 12:30:36', 1);
INSERT INTO `master_access` VALUES (55, 'M_KELUARGA', '', '2020-01-28 14:23:27', 1);
INSERT INTO `master_access` VALUES (56, 'C_KELUARGA', '', '2020-01-28 14:23:36', 1);
INSERT INTO `master_access` VALUES (57, 'R_KELUARGA', '', '2020-01-28 14:23:44', 1);
INSERT INTO `master_access` VALUES (58, 'U_KELUARGA', '', '2020-01-28 14:23:56', 1);
INSERT INTO `master_access` VALUES (59, 'D_KELUARGA', '', '2020-01-28 14:24:06', 1);
INSERT INTO `master_access` VALUES (60, 'M_WARGA', '', '2020-11-23 06:27:56', 1);
INSERT INTO `master_access` VALUES (61, 'C_WARGA', '', '2020-11-23 06:28:06', 1);
INSERT INTO `master_access` VALUES (62, 'R_WARGA', '', '2020-11-23 06:28:13', 1);
INSERT INTO `master_access` VALUES (63, 'U_WARGA', '', '2020-11-23 06:28:17', 1);
INSERT INTO `master_access` VALUES (64, 'D_WARGA', '', '2020-11-23 06:28:22', 1);
INSERT INTO `master_access` VALUES (65, 'M_KATEGORI', '', '2020-12-22 08:31:02', 1);
INSERT INTO `master_access` VALUES (66, 'C_KATEGORI', '', '2020-12-22 08:45:42', 1);
INSERT INTO `master_access` VALUES (67, 'R_KATEGORI', '', '2020-12-22 08:45:51', 1);
INSERT INTO `master_access` VALUES (68, 'U_KATEGORI', '', '2020-12-22 08:45:57', 1);
INSERT INTO `master_access` VALUES (69, 'D_KATEGORI', '', '2020-12-22 08:46:04', 1);

-- ----------------------------
-- Table structure for sy_config
-- ----------------------------
DROP TABLE IF EXISTS `sy_config`;
CREATE TABLE `sy_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `conf_val` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sy_config
-- ----------------------------
INSERT INTO `sy_config` VALUES (3, 'APP_NAME', 'Jawicode Framework', '');
INSERT INTO `sy_config` VALUES (8, 'OPD_NAME', 'Build with Codeigniter 3.1.9 and Harviacode', '');
INSERT INTO `sy_config` VALUES (9, 'LEFT_FOOTER', '<strong>Copyright</strong> Jawicode © 2019', '');
INSERT INTO `sy_config` VALUES (10, 'RIGHT_FOOTER', 'Jawicode Framework', '');
INSERT INTO `sy_config` VALUES (11, 'APP_DESC', 'Build with Codeigniter 3.1.9 and Harviacode', '-');
INSERT INTO `sy_config` VALUES (12, 'OPD_ADDR', 'Jln. xxx 130 Telp. (0293) xxxxx ', '');
INSERT INTO `sy_config` VALUES (13, 'VISI_MISI', 'Build with Codeigniter 3.1.9 and Harviacode', '');
INSERT INTO `sy_config` VALUES (14, 'APP_TELP', '089636555xxx', '');
INSERT INTO `sy_config` VALUES (15, 'APP_EMAIL', 'testes@gmail.co.id', '');
INSERT INTO `sy_config` VALUES (16, 'APP_FB', 'https://www.facebook.com', '');
INSERT INTO `sy_config` VALUES (17, 'APP_TWITTER', 'https://twitter.com', '');
INSERT INTO `sy_config` VALUES (18, 'APP_IG', 'https://instagram.com', '');

-- ----------------------------
-- Table structure for user_access
-- ----------------------------
DROP TABLE IF EXISTS `user_access`;
CREATE TABLE `user_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NULL DEFAULT NULL,
  `kd_access` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nm_access` varbinary(100) NULL DEFAULT NULL,
  `is_allow` int(1) NULL DEFAULT NULL COMMENT '0=false,1=true',
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 160 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_access
-- ----------------------------
INSERT INTO `user_access` VALUES (5, 2, '1', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (8, 1, '1', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (9, 3, '5', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (10, 3, '1', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (11, 3, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (12, 4, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (13, 1, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (14, 1, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (15, 1, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (16, 1, '5', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (17, 1, '6', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (18, 3, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (19, 2, '5', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (20, 4, '5', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (21, 4, '6', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (22, 3, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (23, 4, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (24, 1, '7', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (25, 1, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (26, 1, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (27, 1, '10', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (28, 5, '10', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (29, 5, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (30, 2, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (31, 2, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (32, 1, '14', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (33, 2, '14', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (34, 1, '12', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (35, 2, '12', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (36, 1, '13', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (37, 1, '11', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (38, 5, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (39, 5, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (40, 2, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (41, 2, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (42, 3, '6', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (43, 3, '7', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (44, 3, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (45, 3, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (46, 3, '10', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (47, 3, '11', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (48, 3, '12', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (49, 3, '13', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (50, 4, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (51, 4, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (52, 4, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (53, 5, '15', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (54, 1, '15', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (55, 1, '16', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (56, 6, '2', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (57, 6, '3', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (58, 6, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (59, 6, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (60, 6, '9', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (61, 6, '14', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (62, 6, '15', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (63, 5, '16', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (64, 5, '17', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (65, 5, '18', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (66, 5, '19', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (67, 5, '21', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (68, 6, '18', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (69, 1, '17', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (70, 1, '18', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (71, 1, '19', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (72, 1, '21', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (73, 1, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (74, 2, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (75, 6, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (76, 5, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (77, 5, '8', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (78, 5, '22', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (79, 2, '4', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (80, 1, '23', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (81, 1, '24', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (82, 1, '25', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (83, 1, '26', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (84, 1, '27', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (85, 1, '28', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (86, 1, '29', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (87, 1, '30', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (88, 1, '31', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (89, 1, '32', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (90, 1, '33', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (91, 1, '34', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (92, 1, '35', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (93, 2, '24', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (94, 1, '36', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (95, 1, '37', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (96, 1, '38', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (97, 1, '39', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (98, 1, '40', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (99, 1, '41', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (100, 1, '42', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (101, 1, '43', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (102, 1, '44', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (103, 1, '45', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (104, 1, '46', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (105, 2, '23', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (106, 2, '25', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (107, 2, '26', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (108, 2, '28', NULL, 0, NULL);
INSERT INTO `user_access` VALUES (109, 2, '30', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (110, 2, '31', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (111, 2, '32', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (112, 2, '33', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (113, 2, '34', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (114, 2, '35', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (115, 2, '36', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (116, 2, '37', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (117, 2, '38', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (118, 2, '39', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (119, 2, '40', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (120, 2, '41', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (121, 2, '42', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (122, 2, '43', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (123, 2, '44', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (124, 2, '45', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (125, 2, '16', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (126, 2, '46', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (127, 2, '47', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (128, 2, '50', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (129, 2, '49', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (130, 2, '48', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (131, 2, '54', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (132, 2, '53', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (133, 2, '52', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (134, 2, '51', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (135, 1, '59', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (136, 1, '58', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (137, 1, '57', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (138, 1, '56', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (139, 1, '55', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (140, 2, '64', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (141, 2, '63', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (142, 2, '62', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (143, 2, '61', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (144, 2, '60', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (145, 2, '59', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (146, 2, '58', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (147, 2, '57', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (148, 2, '56', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (149, 2, '55', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (150, 1, '64', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (151, 1, '63', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (152, 1, '62', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (153, 1, '61', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (154, 1, '60', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (155, 1, '65', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (156, 1, '69', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (157, 1, '68', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (158, 1, '67', NULL, 1, NULL);
INSERT INTO `user_access` VALUES (159, 1, '66', NULL, 1, NULL);

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_group
-- ----------------------------
INSERT INTO `user_group` VALUES (1, 'Developer', '-', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
INSERT INTO `user_group` VALUES (2, 'Administrator', 'Admin Aplikasi', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_group` int(11) NULL DEFAULT NULL COMMENT 'fk dari tabel user_group',
  `foto` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telp` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `note_1` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ip` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Developer', 'dev', '227edf7c86c02a44d17eec9aa5b30cd1', 'dev@email.com', 1, 'a4.jpg', '085643242654', 'full akses', 1, 1, '2018-03-13 03:06:55', '2020-01-24 08:36:51', '', '', '2019-08-27 20:12:45');

SET FOREIGN_KEY_CHECKS = 1;
