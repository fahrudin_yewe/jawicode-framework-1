# Jawicode Framework 1 #

Build with Codeigniter 3.1.9 + Harviacode

### Features ###

* Auth
* Access Management
* CRUD Generator include template

### How do I get set up? ###

* copy paste project folder
* import DB (assets/db/)
* setup config.php, database.php 

### Contribution guidelines ###

* Free to use (with watermark on footer page)

### Thanks for contributing ###

