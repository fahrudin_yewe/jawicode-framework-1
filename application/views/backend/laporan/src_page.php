<div class="ibox">
    <div class="ibox-content">
        <form action="<?=base_url()?>laporan/list" method="POST">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="">Kecamatan</label>
                        <select name="kec" id="kec" class="form-control" onchange="srcDesa()">
                            <!-- data option kecamatan tampil disini -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Desa/Kelurahan</label>
                        <select name="desa" id="desa" class="form-control" onchange="srcDusun()">
                            <!-- data option kecamatan tampil disini -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Dusun</label>
                        <select name="dusun" id="dusun" class="form-control">
                            <!-- data option kecamatan tampil disini -->
                        </select>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$(document).ready(function() {
    srcKec();
});

function srcKec() {
    $.post("<?=base_url()?>laporan/getKec", {}, function(data) {
        $("#kec").html(data);
        $("#desa").html("");
        $("#dusun").html("");
    })
}

function srcDesa() {
    $.post("<?=base_url()?>laporan/getDesa", {
        kec: $("#kec").val()
    }, function(data) {
        $("#desa").html(data);
        $("#dusun").html("");
    })
}

function srcDusun() {
    $.post("<?=base_url()?>laporan/getDusun", {
        desa: $("#desa").val()
    }, function(data) {
        $("#dusun").html(data);
    })
}
</script>