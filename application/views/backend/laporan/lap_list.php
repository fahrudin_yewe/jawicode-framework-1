<style>
th {
    text-align: center;
    vertical-align: middle;
}
</style>
<div class="ibox">
    <div class="ibox-content">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="4"> NO URUT RT </th>
                    <th rowspan="4"> NO URUT KK </th>
                    <th rowspan="4"> NAMA KEPALA KELUARGA </th>
                    <th colspan="2" rowspan="3"> KEPALA KELUARGA MENURUT JENIS KELAMIN </th>
                    <th colspan="2" rowspan="3"> KEPALA KELUARGA MENURUT STATUS PEKERJAAN </th>
                    <th colspan="2" rowspan="3"> KEPALA KELUARGA MENURUT STATUS PERKAWINAN </th>
                    <th colspan="4" rowspan="3"> KEPALA KELUARGA MENURUT STATUS PERKAWINAN </th>
                    <th colspan="2" rowspan="3"> JUMLAH JIWA DALAM KELUARGA </th>
                    <th rowspan="4"> JUMLAH WANITA USIA SUBUR (15 - 49 TAHUN)</th>
                    <th colspan="4" rowspan="2"> JUMLAH ANGGOTA KELUARGA </th>
                    <th colspan="6"> JUMLAH ANGGOTA KELUARGA MENURUT KELOMPOK UMUR</th>
                    <th colspan="14"> PASANGAN USIA SUBUR</th>
                </tr>
                <tr>
                    <th rowspan="3"> BAYI >1 THN </th>
                    <th rowspan="3"> BALITA 1- <5 THN </th>
                    <th rowspan="3"> 5 - <10 THN </th>
                    <th rowspan="3"> 10 -< 25 THN </th>
                    <th rowspan="3"> 25 -< 60 THN </th>
                    <th rowspan="3"> 60 THN KE ATAS </th>
                    <th rowspan="3"> NO URUT PUS </th>
                    <th colspan="4"> ISTRI </th>
                    <th colspan="6"> PESERTA JAMINAN KESEHATAN NASIONAL </th>
                    <th colspan="2" rowspan="2"> BUKAN PESERTA JAMINAN KESEHATAN </th>
                    <th rowspan="3"> KB </th>
                </tr>
                <tr>
                    <th colspan="2"> SEKOLAH </th>
                    <th colspan="2"> TDK SEKOLAH </th>
                    <th rowspan="2"> NAMA </th>
                    <th colspan="3"> KELOMPOK UMUR </th>
                    <th colspan="2"> BPJS - PBI </th>
                    <th colspan="2"> BPJS - NON PBI </th>
                    <th colspan="2"> NON BPJS </th>
                </tr>
                <tr>
                    <th> L </th>
                    <th> P </th>
                    <th> BEKERJA </th>
                    <th> TDK BEKERJA </th>
                    <th> KAWIN </th>
                    <th> D/J/BK </th>
                    <th> TDK TAMAT SD </th>
                    <th> TAMAT SD - SLTP </th>
                    <th> TAMAT SLTA </th>
                    <th> TAMAT AK/PT </th>
                    <th> L </th>
                    <th> P </th>
                    <th> L </th>
                    <th> P </th>
                    <th> L </th>
                    <th> P </th>
                    <th> >20 TH </th>
                    <th> 20 - 29 TH </th>
                    <th> 30 - 49 TH </th>
                    <th> BUKAN PERSERTA KB </th>
                    <th> PESERTA KB </th>
                    <th> BUKAN PERSERTA KB </th>
                    <th> PESERTA KB </th>
                    <th> BUKAN PERSERTA KB </th>
                    <th> PESERTA KB </th>
                    <th> BUKAN PERSERTA KB </th>
                    <th> PESERTA KB </th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach($data as $k => $v){ ?>
                <tr>
                    <td><?=$v->rt?></td>
                    <td></td>
                    <td><?=$v->nama?></td>
                    <td class="text-center"><?=$v->jk=="M"?1:"-"?></td>
                    <td class="text-center"><?=$v->jk=="F"?1:"-"?></td>
                    <td class="text-center"><?=$v->pekerjaan!="BELUM/TIDAK BEKERJA"?1:"-"?></td>
                    <td class="text-center"><?=$v->pekerjaan=="BELUM/TIDAK BEKERJA"?1:"-"?></td>
                    <td class="text-center"><?=$v->status_perkawinan=="KAWIN"?1:"-"?></td>
                    <td class="text-center"><?=$v->status_perkawinan!="KAWIN"?1:"-"?></td>
                    <td class="text-center"><?=$v->pendidikan=="BELUM TAMAT SD/SEDERAJAT"?1:"-"?></td>
                    <td class="text-center">
                        <?=($v->pendidikan=="TAMAT SD SEDERAJAT"||$v->pendidikan=="SLTP / SEDERAJAT")?1:"-"?></td>
                    <td class="text-center"><?=$v->pendidikan=="SLTA / SEDERAJAT"?1:"-"?></td>
                    <td class="text-center">
                        <?=($v->pendidikan=="D I/ II"||$v->pendidikan=="AKADEMI/DIPLOMA III/ SARJANA MUDA"||$v->pendidikan=="DIPLOMA IV/STRATA I"||$v->pendidikan=="STRATA-II"||$v->pendidikan=="STRATA-III")?1:"-"?>
                    </td>
                    <td class="text-center"><?=$v->tot_laki?></td>
                    <td class="text-center"><?=$v->tot_perempuan?></td>
                    <td></td>
                    <td class="text-center"><?=$v->jml_laki_sekolah?></td>
                    <td class="text-center"><?=$v->jml_perempuan_sekolah?></td>
                    <td class="text-center"><?=$v->jml_laki_tdk_sekolah?></td>
                    <td class="text-center"><?=$v->jml_perempuan_tdk_sekolah?></td>
                    <td><?=$v->umur1?></td>
                    <td><?=$v->umur5?></td>
                    <td><?=$v->umur10?></td>
                    <td><?=$v->umur25?></td>
                    <td><?=$v->umur60?></td>
                    <td><?=$v->umur60keatas?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?=@$v->bpjs_pbi_nonkb=="77"?"V":"-"?></td>
                    <td><?=@$v->bpjs_pbi_nonkb=="78"?"V":"-"?></td>
                    <td><?=@$v->bpjs_pbi_nonkb=="79"?"V":"-"?></td>
                    <td><?=@$v->bpjs_pbi_nonkb=="80"?"V":"-"?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>