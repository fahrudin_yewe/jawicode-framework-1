<style>
	th{
		text-align: center;
		vertical-align: middle;
	}
</style>
<div class="ibox">
<div class="ibox-content">
	<table border="1" class="table table-bordered">
	<thead>
		<tr>
			<th rowspan="4">NO URUT</th>
			<th rowspan="4">RT</th>
			<th colspan="2" rowspan="2">JUMLAH</th>
			<th colspan="2" rowspan="2">KEPALA KELUARGA MENURUT JENIS KELAMIN</th>
			<th colspan="2" rowspan="2">KEPALA KELUARGA MENURUT STATUS PEKERJAAN</th>
			<th colspan="2" rowspan="2">KEPALA KELUARGA MENURUT STATUS PERKAWINAN</th>
			<th colspan="4" rowspan="2">KEPALA KELUARGA MENURUT TINGKAT PENDIDIKAN</th>
			<th colspan="3" rowspan="2">JUMLAH JIWA DALAM KELUARGA</th>
			<th rowspan="4">JUMLAH WANITA USIA SUBUR (15 - 49 TAHUN)
</th>
			<th colspan="4">JUMLAH ANGGOTA KELUARGA 7-15 th</th>
			<th colspan="6">JUMLAH ANGGOTA KELUARGA MENURUT KELOMPOK UMUR</th>
			<th colspan="26">PASANGAN USIA SUBUR</th>
			
		</tr>
		<tr>
			<th colspan="2">SEKOLAH</th>
			<th colspan="2">TIDAK SEKOLAH</th>
			<th rowspan="3">BAYI <1 TAHUN</th>
			<th rowspan="3">BALITA 1-<5 thN</th>
			<th rowspan="3">5-<10th</th>
			<th rowspan="3">10-<25th</th>
			<th rowspan="3">25-<60 th</th>
			<th rowspan="3">60 th KE ATAS</th>
			<th rowspan="3">PUS</th>
			<th colspan="3">KELOMPOK UMUR</th>
			<th colspan="6">PESERTA JAMINAN KESEHATAN NASIONAL</th>
			<th colspan="2" rowspan="2">BUKAN PESERTA JAMINAN KESEHATAN NASIONAL</th>
			<th colspan="9">PESERTA KB			</th>
			<th colspan="4" rowspan="2">BUKAN PESERTA KB</th>
			<th rowspan="3">%PA</th>
		</tr>
		<tr>
			<th rowspan="2">RUMAH TANGGA</th>
			<th rowspan="2">KELUARGA</th>
			<th rowspan="2">LAKI-LAKI</th>
			<th rowspan="2">PEREMPUAN</th>
			<th rowspan="2">BEKERJA</th>
			<th rowspan="2">TIDAK BEKERJA</th>
			<th rowspan="2">KAWIN</th>
			<th rowspan="2">DUDA/JANDA/BK</th>
			<th rowspan="2">TDK TAMAT SD</th>
			<th rowspan="2">TAMAT SD - SLTP</th>
			<th rowspan="2">TAMAT SLTA</th>
			<th rowspan="2">TAMAT AK/PT</th>
			<th rowspan="2">LAKI-LAKI</th>
			<th rowspan="2">PEREMPUAN</th>
			<th rowspan="2">JUMLAH</th>
			<th rowspan="2">LAKI-LAKI</th>
			<th rowspan="2">PEREMPUAN</th>
			<th rowspan="2">LAKI-LAKI</th>
			<th rowspan="2">PEREMPUAN</th>
			<th rowspan="2"><20 th</th>
			<th rowspan="2">20 - 29 th</th>
			<th rowspan="2">30 - 49 th</th>
			<th colspan="2">BPJS - PBI	</th>
			<th colspan="2">BPJS - NON PBI</th>
			<th colspan="2">NON BPJS</th>
			<th colspan="9">METODE KONTRASEPSI YANG DIPAKAI								
</th>
		</tr>
		<tr>
			<th>PESERTA KB</th>
			<th>BUKAN PESERTA KB</th>
			<th>PESERTA KB</th>
			<th>BUKAN PESERTA KB</th>
			<th>PESERTA KB</th>
			<th>BUKAN PESERTA KB</th>
			<th>PESERTA KB</th>
			<th>BUKAN PESERTA KB</th>
			<th>IUD</th>
			<th>MOW</th>
			<th>MOP</th>
			<th>K</th>
			<th>IP</th>
			<th>S</th>
			<th>P</th>
			<th>T</th>
			<th>JML</th>
			<th>H</th>
			<th>IAS</th>
			<th>IAT</th>
			<th>TIAL</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($data as $k => $v){ ?>
		<tr>
			<td><?=$k+1?></td>
			<td><?=$v->rt?></td>
			<td><?=$v->jml_rt?></td>
			<td></td>
			<td><?=$v->jml_kepala_laki?></td>
			<td><?=$v->jml_kepala_perempuan?></td>
			<td><?=$v->jml_kepala_bekerja?></td>
			<td><?=$v->jml_kepala_tdk_bekerja?></td>
			<td><?=$v->jml_kepala_kawin?></td>
			<td><?=$v->jml_kepala_bkn_kawin?></td>
			<td><?=$v->jml_tdk_tamat_sd?></td>
			<td><?=$v->jml_tamat_sd_sltp?></td>
			<td><?=$v->jml_tamat_slta?></td>
			<td><?=$v->jml_tamat_ak_pt?></td>
		</tr>
<?php } ?>
	</tbody>
</table>
</div>