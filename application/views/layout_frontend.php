	<?php
$this->load->model('Auth_model');

$pengumuman = "";
$berita     = "";
$totSiswa   = "";
$totAlumni  = "";
$totPtk     = "";
?>
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?=base_url()?>assets/vendor/education/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Education</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/linearicons.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/bootstrap.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/magnific-popup.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/nice-select.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/animate.min.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/owl.carousel.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/jquery-ui.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/vendor/education/css/main.css">
		</head>
		<!-- overflow: hidden; => make window not scrollable -->
		<body>

			<body>
				<header id="header" id="home" class="header-scrolled">
					<div class="header-top">
						<div class="container">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
									<ul>
										<li><a href="<?=data_app('APP_FB')?>"><i class="fa fa-facebook"></i></a></li>
										<li><a href="<?=data_app('APP_TWITTER')?>"><i class="fa fa-twitter"></i></a></li>
										<li><a href="<?=data_app('APP_IG')?>"><i class="fa fa-instagram"></i></a></li>
									</ul>
								</div>
								<div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
									<a href="tel:<?=data_app('APP_TELP')?>"><span class="lnr lnr-phone-handset"></span> <span class="text"><?=data_app('APP_TELP')?></span></a>
									<a href="mailto:<?=data_app('APP_EMAIL')?>"><span class="lnr lnr-envelope"></span> <span class="text"><?=data_app('APP_EMAIL')?></span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="container main-menu">
						<div class="row align-items-center justify-content-between d-flex">
							<div id="logo">
								<a href="index.html"><img src="<?=base_url()?>assets/vendor/education/img/logo.png" alt="" title="" style="width: 50px;"/></a>
							</div>
							<nav id="nav-menu-container">
								<ul class="nav-menu">
									<li><a href="<?=base_url()?>frontend">Home</a></li>
									<li class="menu-has-children"><a href="#">Menu</a>
										<ul>
											<li><a href="#">Menu Lainnya</a></li>
										</ul>
									</li>
									<li><a href="<?=base_url()?>frontend/login">Login</a></li>

									<li><a href="<?=base_url()?>frontend/hubungi_kami">Hubungi Kami</a></li>
								</ul>
							</nav><!-- #nav-menu-container -->
						</div>
					</div>
				</header><!-- #header -->




				<script src="<?=base_url()?>assets/vendor/education/js/vendor/jquery-2.2.4.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/vendor/bootstrap.min.js"></script>
				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/easing.min.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/hoverIntent.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/superfish.min.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/jquery.ajaxchimp.min.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/jquery.magnific-popup.min.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/jquery.tabs.min.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/jquery.nice-select.min.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/owl.carousel.min.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/mail-script.js"></script>
				<script src="<?=base_url()?>assets/vendor/education/js/main.js"></script>
				</html>