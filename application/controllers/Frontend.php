<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Auth_model');

    }

    public function index() {
        $data = [
            'content' => 'frontend/index.php',
        ];
        $this->load->view('layout_frontend.php', $data);
    }

    public function hubungi_kami() {
        $data = [
            'content' => "frontend/hubungi_kami/hubungi_kami_index",
        ];
        $this->load->view(layout('front'), $data);
    }

    public function pengumuman() {
        $data = [
            'data_pengumuman' => $this->Pengumuman_model->get_all(),
            'content'         => "frontend/pengumuman/pengumuman_list",
        ];
        $this->load->view(layout('front'), $data);
    }

    public function video() {
        $last_id_video = $this->db->select_max('id_video')->get('video')->row()->id_video;
        $data          = [
            'data_video' => $this->Video_model->get_all(),
            'last_video' => $this->db->where("id_video", $last_id_video)->get('video')->row(),
            'content'    => "frontend/video/video_list",
        ];
        $this->load->view(layout('front'), $data);
    }

    public function galeri() {
        $data = [
            'data_galeri' => $this->Galeri_model->get_all(),
            'content'     => "frontend/galeri/galeri_list",
        ];
        $this->load->view(layout('front'), $data);
    }

    public function list_berita() {
        $data = [
            'data_berita' => $this->Berita_model->get_all(),
            'content'     => "frontend/berita/list_berita",
        ];
        $this->load->view(layout('front'), $data);
    }

    public function single_berita($id) {
        $data = [
            'single_berita' => $this->Berita_model->get_by_id($id),
            'content'       => "frontend/berita/single_berita",
        ];
        $this->load->view(layout('front'), $data);
    }

    public function login() {
        $data = [
            // 'content'=>'frontend/login.php'
        ];
        $this->load->view('frontend/login.php', $data);
    }

}
